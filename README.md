# Getting Started with Getir-Market

Getir-Market has been developed as an application where products can be listed by pagination, brand-based, label-based and sorting filters can be made, and products can be added to and removed from the basket.

While developing the application, react was used as the javascript library and redux was used as the state management tool.

#### Production

* Application will be deployed on change master branch.
* Deployed Url: https://deploy-market-prod.herokuapp.com

## Directory Structure

* At the beginning of application development, common components were determined and added to the file named `/components`.
* All styles imported under `/assets` folder'
* Getir Market has just one page so there is one file under `page` directory.
* Application redux actions, stores and reducers are added under `store`. Actions and reducers are kept together in a single file because extensive functions are not required.
* Common types declared under the folder `types`.

## Notes

* Main page consists of 3 sections: Filters, Products and Basket. (Can be found under `/page/market`). And common components which are exported from `/components` could be used in these sections.
* All components created as functional component to use react hooks components state.
* Ant Design used as component library.
## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `json-server`

Runs json-server locally and serves web services.

## API'S

- dev/test: http://localhost:3004
- production: https://market-getir-api.herokuapp.com

##### Web Services
* `/companies`
* `/products`
* `/tags` 

**Note: Since there is no separate data about the tags, all the tags in the products have been taken and turned into a separate web service as `/tags`.**


## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
