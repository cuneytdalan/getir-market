import {FunctionComponent} from "react";
import {useSelector} from "react-redux";
import {getTotalPrice} from "../../store/basket";

export const TotalPrice: FunctionComponent = () => {
    const price = useSelector(getTotalPrice)
    return <div className="total-price-container">₺ {price || '0.00'}</div>
}
