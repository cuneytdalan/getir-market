import {Checkbox, Input, Row, Col} from "antd";
import {FunctionComponent, PropsWithChildren, useState} from "react";
import {FilterOptions} from "../../types/options";
import {MarketLoader} from "../loader";

export type MarketFilterListOptionsType = {
    label: string;
    value: string | number;
}

type MarketFilterListComponentType = {
    options: FilterOptions<any>[];
    loading: boolean;
    placeholder: string;
    onChangedFilterList: any;
}

export const MarketFilterListComponent: FunctionComponent<MarketFilterListComponentType> = (props: PropsWithChildren<MarketFilterListComponentType>) => {
    const {options, loading, placeholder, onChangedFilterList} = props;
    const [filteredOptions, setFilteredOptions] = useState(options);

    const onChange = (checkedValues: any) => {
        onChangedFilterList(checkedValues);
    }

    const handleOnSearch = (e: any) => {
        const filteredData = options.filter(item => item.label.toLowerCase().includes(e.target.value.toLowerCase()))
        setFilteredOptions(filteredData)
    }

    return <div className="market-filter-list-container">
        <div className="market-filter-list-search">
            <Input placeholder={placeholder} onInput={handleOnSearch}/>
        </div>
        <div className="market-filter-list">
            {<Checkbox.Group style={{width: '100%'}} onChange={onChange}>
                {filteredOptions.map((item, i) => {
                    return <Row key={i}>
                        <Col span={24}>
                            <Checkbox value={item.value}>{item.label}</Checkbox>
                        </Col>
                    </Row>
                })}
            </Checkbox.Group>}
            {
                loading && <MarketLoader/>
            }
        </div>
    </div>
}
