import React, {FunctionComponent} from 'react'
import { Header } from './header';

type GeneralPageLayoutProps = {
    children: any;
}

export const GeneralPageLayout: FunctionComponent<GeneralPageLayoutProps> = (props: GeneralPageLayoutProps) => {
    const {children} = props;
    return <div>
        <Header/>
        <div>{children}</div>
    </div>
}
