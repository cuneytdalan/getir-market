import {FunctionComponent} from "react";

export const GeneralContent: FunctionComponent<any> = (props) => {
    const {children} = props;
    return <div>{children}</div>
}
