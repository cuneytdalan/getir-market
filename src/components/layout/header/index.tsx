import {FunctionComponent} from "react";
import {MarketLogo} from "../appLogo";
import {TotalPrice} from "../../totalPrice";

export const Header: FunctionComponent = () => {
    return <div className="header">
        <MarketLogo/>
        <TotalPrice/>
    </div>
}

