import Radio from "antd/lib/radio";
import {FunctionComponent, useEffect, useState} from "react";
import {Space} from "antd";

export type MarketRadioButtonDataType = {
    label: string;
    value: any;
}

type MarketRadioButtonType = {
    direction?: 'vertical' | 'horizontal';
    data: MarketRadioButtonDataType[];
    onHandleSelection: any
}

export const MarketRadioButton: FunctionComponent<MarketRadioButtonType> = (props) => {
    const {direction, data, onHandleSelection} = props;
    const [value, setValue] = useState(null);

    useEffect(() => {
        if (!value) {
            return;
        }
        onHandleSelection(value);
    }, [value]);

    const onChange = (e: any) => {
        setValue(e.target.value);
    };

    return <Radio.Group onChange={onChange} value={value}>
        <Space direction={direction || 'vertical'}>
            {data.map(item => <Radio key={item.label} value={item.value}>{item.label}</Radio>)}
        </Space>
    </Radio.Group>
}
