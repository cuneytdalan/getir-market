import {Spin} from "antd";

export const MarketLoader = () => {
    return <div className="market-loader">
        <Spin/>
    </div>
}
