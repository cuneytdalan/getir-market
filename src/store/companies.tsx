import {getCompaniesApi} from "../api/apiCall";

const initialState = {
    data: null,
    meta: {
        requested: false,
        success: false,
        failure: false
    }
}

//reducer
export function companiesReducer(state = initialState, action: any) {
    switch (action.type) {
        case COMPANIES_REQUESTED: {
            const newState = state;
            newState.meta.requested = true;
            newState.meta.success = false;
            newState.meta.failure = false;
            return Object.assign({}, newState)
        }
        case COMPANIES_SUCCESS: {
            const newState = state;
            newState.meta.requested = false;
            newState.meta.success = true;
            newState.meta.failure = false;
            newState.data = action.payload;
            return Object.assign({}, newState)
        }
        case COMPANIES_FAILURE: {
            const newState = state;
            newState.meta.requested = false;
            newState.meta.success = false;
            newState.meta.failure = true;
            return Object.assign({}, newState)
        }
        default:
            return state;
    }
}

//action types
export const COMPANIES_SUCCESS = 'COMPANIES_SUCCESS';
export const COMPANIES_REQUESTED = 'COMPANIES_REQUESTED';
export const COMPANIES_FAILURE = 'COMPANIES_FAILURE';

//selectors
export const getCompanies = (state: any) => state.companies.data;

//actions
export function getCompaniesData() {
    return (dispatch: any) => {
        dispatch({type: COMPANIES_REQUESTED});
        getCompaniesApi().then(companies => {
            dispatch({
                type: COMPANIES_SUCCESS,
                payload: companies
            })
        }).catch(error => {
            console.log(error);
            dispatch({type: COMPANIES_FAILURE})
        })
    }
}

