import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";

import {basketReducer} from "./basket";
import {companiesReducer} from "./companies";
import {productsReducer} from "./products";
import {filtersReducer} from "./filters";
import {tagsReducer} from "./tags";

export const store = createStore(
    combineReducers({
        basket: basketReducer,
        products: productsReducer,
        companies: companiesReducer,
        filters: filtersReducer,
        tags: tagsReducer
    }),
    applyMiddleware(thunk)
)
