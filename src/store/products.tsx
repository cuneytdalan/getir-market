import {getItemsApi} from "../api/apiCall";

const initialState = {
    data: null,
    meta: {
        requested: false,
        success: false,
        failure: false
    }
}

//reducer
export function productsReducer(state = initialState, action: any) {
    switch (action.type) {
        case PRODUCTS_REQUESTED: {
            const newState = state;
            newState.meta.requested = true;
            newState.meta.success = false;
            newState.meta.failure = false;
            return Object.assign({}, newState)
        }
        case PRODUCTS_SUCCESS: {
            const newState = state;
            newState.meta.requested = false;
            newState.meta.success = true;
            newState.meta.failure = false;
            newState.data = action.payload;
            return Object.assign({}, newState)
        }
        case PRODUCTS_FAILURE: {
            const newState = state;
            newState.meta.requested = false;
            newState.meta.success = false;
            newState.meta.failure = true;
            return Object.assign({}, newState)
        }
        default:
            return state;
    }
}

//action types
export const PRODUCTS_REQUESTED = "PRODUCTS_REQUESTED"
export const PRODUCTS_SUCCESS = "PRODUCTS_SUCCESS"
export const PRODUCTS_FAILURE = "PRODUCTS_FAILURE"

//selector
export const getProducts = (state: any) => state.products.data;
export const getLoading = (state: any) => state.products.meta.requested;

//actions
export function getProductsData(pageInfo: any) {
    return (dispatch: any) => {
        dispatch({type: PRODUCTS_REQUESTED});
        getItemsApi(pageInfo).then(response => {
            dispatch({type: PRODUCTS_SUCCESS, payload: response})
        }).catch(error => {
            console.log(error);
            dispatch({type: PRODUCTS_FAILURE})
        })
    }
}
