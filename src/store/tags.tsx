import {getTagsApi} from "../api/apiCall";

const initialState = {
    data: null,
    meta: {
        requested: false,
        success: false,
        failure: false
    }
}

//reducer
export function tagsReducer(state = initialState, action: any) {
    switch (action.type) {
        case TAGS_REQUESTED: {
            const newState = state;
            newState.meta.requested = true;
            newState.meta.success = false;
            newState.meta.failure = false;
            return Object.assign({}, newState)
        }
        case TAGS_SUCCESS: {
            const newState = state;
            newState.meta.requested = false;
            newState.meta.success = true;
            newState.meta.failure = false;
            newState.data = action.payload;
            return Object.assign({}, newState)
        }
        case TAGS_FAILURE: {
            const newState = state;
            newState.meta.requested = false;
            newState.meta.success = false;
            newState.meta.failure = true;
            return Object.assign({}, newState)
        }
        default:
            return state;
    }
}

//action types
export const TAGS_REQUESTED = "TAGS_REQUESTED";
export const TAGS_SUCCESS = "TAGS_SUCCESS";
export const TAGS_FAILURE = "TAGS_FAILURE";

//selector
export const getTags = (state: any) => state.tags.data;
export const getTagsLoading = (state: any) => state.tags.meta.requested;

//actions
export function getTagsData() {
    return (dispatch: any) => {
        dispatch({type: TAGS_REQUESTED});
        getTagsApi().then((response: string[]) => {
            dispatch({type: TAGS_SUCCESS, payload: response})
        }).catch(error => {
            console.log(error);
            dispatch({type: TAGS_FAILURE})
        })
    }
}
