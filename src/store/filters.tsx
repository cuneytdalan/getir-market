import {PageInfo} from "../types/pageOptions";
import {ITEM_TYPE} from "../page/market/products";

const initialState = {
    selectedSorting: {order: null, sort: null},
    selectedBrands: [],
    selectedTags: [],
    selectedItemType: null,
    pageInfo: {page: 1, limit: 16}
}

//action types
const SET_SELECTED_SORTING = "SET_SELECTED_SORTING";
const SET_PAGE_INFO = "SET_PAGE_INFO";
const SET_SELECTED_BRANDS = "SET_SELECTED_BRANDS";
const SET_SELECTED_ITEM_TYPE = 'SET_SELECTED_ITEM_TYPE';
const SET_SELECTED_TAGS = 'SET_SELECTED_TAGS';

//reducer
export function filtersReducer(state = initialState, action: any) {
    switch (action.type) {
        case SET_SELECTED_SORTING: {
            const newState = state;
            newState.selectedSorting = action.payload;
            return Object.assign({}, newState)
        }
        case SET_PAGE_INFO: {
            const newState = state;
            newState.pageInfo = action.payload;
            return Object.assign({}, newState);
        }
        case SET_SELECTED_BRANDS: {
            const newState = state;
            newState.selectedBrands = action.payload;
            return Object.assign({}, newState);
        }
        case SET_SELECTED_ITEM_TYPE: {
            const newState = state;
            newState.selectedItemType = action.payload;
            return Object.assign({}, newState);
        }
        case SET_SELECTED_TAGS: {
            const newState = state;
            newState.selectedTags = action.payload;
            return Object.assign({}, newState);
        }
        default:
            return state;
    }
}

//actions
export function setSelectedSorting(selectedSorting: any) {
    return {
        type: SET_SELECTED_SORTING,
        payload: selectedSorting
    }
}

export function setPageInfo(pageInfo: PageInfo) {
    return {
        type: SET_PAGE_INFO,
        payload: pageInfo
    }
}

export function setSelectedBrands(brands: string[]) {
    return {
        type: SET_SELECTED_BRANDS,
        payload: brands
    }
}

export function setSelectedItemType(type: ITEM_TYPE | null) {
    return {
        type: SET_SELECTED_ITEM_TYPE,
        payload: type
    }
}

export function setSelectedTags(tags: string[]) {
    return {
        type: SET_SELECTED_TAGS,
        payload: tags
    }
}
