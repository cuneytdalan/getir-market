import {Product} from "../types/products";
import {getTotalPriceOfBasketItems} from "../utils/functions";

export type ProductStoreType = {
    item: Product,
    amount: number
}

const initialState: { list: ProductStoreType[], totalPrice: number } = {
    list: [],
    totalPrice: 0
}


export function basketReducer(state = initialState, action: any) {
    switch (action.type) {
        case ADD_BASKET_ITEM: {
            const newState = state;
            const items = newState.list.map(data => data.item)
            if (items.includes(action.payload)) {
                const itemIndex = items.indexOf(action.payload);
                newState.list[itemIndex].amount++
            } else {
                newState.list.push({item: action.payload, amount: 1});
            }
            return Object.assign({}, getTotalPriceOfBasketItems(newState))
        }
        case DECREASE_BASKET_ITEM_AMOUNT: {
            const newState = state;
            const stateIndex = newState.list.indexOf(action.payload);
            if (stateIndex < 0) {
                return
            }
            if (action.payload.amount === 1) {
                newState.list.splice(stateIndex, 1);
            } else {
                newState.list[stateIndex].amount--
            }
            return Object.assign({}, getTotalPriceOfBasketItems(newState));
        }
        case INCREASE_BASKET_ITEM_AMOUNT: {
            const newState = state;
            const stateIndex = newState.list.indexOf(action.payload);
            newState.list[stateIndex].amount++
            return Object.assign({}, getTotalPriceOfBasketItems(newState));
        }
        default:
            return state;
    }
}

export const getTotalPrice = (state: any) => state.basket.totalPrice;
export const getBasketItems = (state: any) => state.basket;

export const ADD_BASKET_ITEM = "ADD_BASKET_ITEM";
export const INCREASE_BASKET_ITEM_AMOUNT = "INCREASE_BASKET_ITEM_AMOUNT";
export const DECREASE_BASKET_ITEM_AMOUNT = "DECREASE_BASKET_ITEM_AMOUNT";

export const addBasketItem = (product: Product) => ({
    type: ADD_BASKET_ITEM,
    payload: product
})

export const decreaseAmountOfBasketItem = (item: ProductStoreType) => ({
    type: DECREASE_BASKET_ITEM_AMOUNT,
    payload: item
})

export const increaseAmountOfBasketItem = (item: ProductStoreType) => ({
    type: INCREASE_BASKET_ITEM_AMOUNT,
    payload: item
})

