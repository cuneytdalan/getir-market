import {FunctionComponent, useEffect} from "react";
import {FilterProduct} from "./filterProduct";
import {Products} from "./products";
import {Basket} from "./basket";
import {useDispatch, useSelector} from "react-redux";
import {getLoading, getProducts, getProductsData} from "../../store/products";

export const MarketPage: FunctionComponent<any> = () => {
    const dispatch = useDispatch();
    const products = useSelector(getProducts);
    const loading = useSelector(getLoading);
    const filters = useSelector((state: any) => state.filters);

    useEffect(() => {
        dispatch(getProductsData(filters));
    }, [filters])

    return <div className="market-container">
        <FilterProduct/>
        <Products products={products} loading={loading}/>
        <Basket/>
    </div>
}
