import {FunctionComponent} from "react";
import {MarketFilterListComponent} from "../../../../components/marketFilterListComponent";
import {FilterProductItem} from "../filterProductItem";
import {FilterOptions} from "../../../../types/options";
import {useDispatch} from "react-redux";
import {setSelectedTags} from "../../../../store/filters";
import {MarketLoader} from "../../../../components/loader";

type TagsFilterType = {
    loading: boolean;
    tags: FilterOptions<string>[]
}

export const TagsFilter: FunctionComponent<TagsFilterType> = (props) => {
    const {tags, loading} = props;
    const dispatch = useDispatch();

    if (!tags || tags.length === 0) {
        return <MarketLoader/>
    }

    const onTagFilterChanged = (data: string[]) => {
        dispatch(setSelectedTags(data))
    }

    return <FilterProductItem label="Tags">
        <MarketFilterListComponent onChangedFilterList={onTagFilterChanged}
                                   options={tags}
                                   loading={loading}
                                   placeholder="Search tag"/>
    </FilterProductItem>
}
