import {FunctionComponent} from "react";

type FilterProductItemType = {
    children: any;
    label: string;
}

export const FilterProductItem: FunctionComponent<FilterProductItemType> = (props) => {
    const {children, label} = props;
    return <div className="filter-product-item-container">
        <label>{label}</label>
        <div className="filter-product-item">
            {children}
        </div>
    </div>

}
