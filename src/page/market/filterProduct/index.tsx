import {FunctionComponent, useEffect, useState} from "react";
import {SortingProduct} from "./sorting";
import {MarketRadioButtonDataType} from "../../../components/marketRadioButton";
import {BrandsFilter} from "./brands";
import {TagsFilter} from "./tags";
import {useDispatch, useSelector} from "react-redux";
import {getCompanies, getCompaniesData} from "../../../store/companies";
import {Company} from "../../../types/company";
import {FilterOptions} from "../../../types/options";
import {getTags, getTagsData, getTagsLoading} from "../../../store/tags";

export const SORTING_VALUES = {
    LOW_TO_HIGH: {sort: 'price', order: 'asc'},
    HIGH_TO_LOW: {sort: 'price', order: 'desc'},
    NEW_TO_OLD: {sort: 'added', order: 'desc'},
    OLD_TO_NEW: {sort: 'added', order: 'asc'}
}

export const FilterProduct: FunctionComponent = () => {
    const dispatch = useDispatch();
    const companies = useSelector(getCompanies);
    const tags = useSelector(getTags);
    const tagsLoading = useSelector(getTagsLoading)
    const [companiesOptions, setCompaniesOptions] = useState<FilterOptions<Company>[]>([]);
    const [tagsOptions, setTagOptions] = useState<FilterOptions<string>[]>([]);


    const sortData: MarketRadioButtonDataType[] = [
        {label: "Price low to high", value: SORTING_VALUES.LOW_TO_HIGH},
        {label: "Price high to low", value: SORTING_VALUES.HIGH_TO_LOW},
        {label: "New to old", value: SORTING_VALUES.NEW_TO_OLD},
        {label: "Old to new", value: SORTING_VALUES.OLD_TO_NEW},
    ]

    useEffect(() => {
        if (companiesOptions.length === 0) {
            dispatch(getCompaniesData());
        }
        if (tagsOptions.length === 0) {
            dispatch(getTagsData());
        }
    }, [])

    useEffect(() => {
        if (companies) {
            setCompaniesOptions(mapCompanies(companies))
        }
    }, [companies])

    useEffect(() => {
        if (companies) {
            setTagOptions(mapTags(tags))
        }
    }, [tags])

    const mapTags = (tags: string[]) => {
        const tagsFilterOptions: FilterOptions<string>[] = [];
        tags.forEach((tag: string) => {
            tagsFilterOptions.push({label: tag, value: tag});
        })
        return tagsFilterOptions;
    }

    const mapCompanies = (companies: Company[]): FilterOptions<Company>[] => {
        const companyFilterOptions: FilterOptions<Company>[] = [];
        companies.forEach((company: Company) => {
            companyFilterOptions.push({label: company.name, value: company});
        })
        return companyFilterOptions;
    }

    return <div className="filter-product-container">
        <SortingProduct sortData={sortData}/>
        <BrandsFilter brands={companiesOptions}/>
        <TagsFilter tags={tagsOptions} loading={tagsLoading}/>
    </div>
}
