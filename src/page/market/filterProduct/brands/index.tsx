import {FunctionComponent} from "react";
import {useDispatch} from "react-redux";
import {MarketFilterListComponent} from "../../../../components/marketFilterListComponent";
import {FilterProductItem} from "../filterProductItem";
import {Company} from "../../../../types/company";
import {FilterOptions} from "../../../../types/options";

import {setSelectedBrands} from "../../../../store/filters";
import {MarketLoader} from "../../../../components/loader";

type BrandsFilterType = {
    brands: FilterOptions<Company>[]
}

export const BrandsFilter: FunctionComponent<BrandsFilterType> = (props) => {
    const {brands} = props;
    const dispatch = useDispatch();

    if (!brands || brands.length === 0) {
        return <MarketLoader/>
    }

    const onSelectBrandsChanged = (brands: Company[]) => {
        const brandNames: string[] = brands.map(brand => brand.slug);
        dispatch(setSelectedBrands(brandNames))
    }

    const renderFilterContent = () => {
        if (!brands || brands.length === 0) {
            return <MarketLoader/>
        }
        return <MarketFilterListComponent onChangedFilterList={onSelectBrandsChanged}
                                          options={brands}
                                          loading={false}
                                          placeholder="Search brand"/>;
    }

    return <FilterProductItem label="Brands">
        {renderFilterContent()}
    </FilterProductItem>
}
