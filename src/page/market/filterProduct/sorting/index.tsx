import {FunctionComponent} from "react";
import {MarketRadioButton} from "../../../../components/marketRadioButton";
import {FilterProductItem} from "../filterProductItem";
import {useDispatch} from "react-redux";
import {setSelectedSorting} from "../../../../store/filters"

type SortingType = {
    sortData: any;
}

export const SortingProduct: FunctionComponent<SortingType> = (props) => {
    const dispatch = useDispatch();
    const {sortData} = props;

    const onHandleChange = (value: any) => {
        dispatch(setSelectedSorting(value))
    }

    return <FilterProductItem label="Sorting">
        <MarketRadioButton data={sortData} onHandleSelection={onHandleChange}/>
    </FilterProductItem>
}
