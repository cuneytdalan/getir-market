import {Button} from "antd";
import {FunctionComponent} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    decreaseAmountOfBasketItem,
    getBasketItems,
    increaseAmountOfBasketItem
} from "../../../store/basket";
import {Product} from "../../../types/products";

export const Basket: FunctionComponent = () => {
    const dispatch = useDispatch();
    const basketProducts = useSelector(getBasketItems);

    const handleDecreaseAmount = (data: { item: Product, amount: number }) => {
        return () => {
            dispatch(decreaseAmountOfBasketItem(data))
        }
    }


    const handleIncreaseAmount = (data: { item: Product, amount: number }) => {
        return () => {
            dispatch(increaseAmountOfBasketItem(data))
        }
    }

    if (basketProducts.list.length === 0) {
        return <div className="basket-container">
            <div className="basket-item-container">
                No products added.
            </div>
        </div>
    }

    return <div className="basket-container">
        {basketProducts.list.map((data: { item: Product, amount: number }, i: number) =>
            <div key={i} className="basket-item-container">
                <div className="basket-item-info">
                    <span className="item-name">{data.item.name}</span>
                    <span className="item-price">{data.item.price}</span>
                </div>
                <div className="item-add-wrapper">
                    <Button onClick={handleDecreaseAmount(data)} type="text">-</Button>
                    <div className="item-amount">{data.amount}</div>
                    <Button onClick={handleIncreaseAmount(data)} type="text">+</Button>
                </div>
            </div>
        )}

        <div className="total-amount-container">
            <div className="total-amount">₺ {basketProducts.totalPrice}</div>
        </div>
    </div>
}
