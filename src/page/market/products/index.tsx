import {FunctionComponent, useState} from "react";
import {ProductItem} from "./productItem/productItem";
import {Product} from "../../../types/products";
import {Button, Pagination} from "antd";
import {useDispatch} from "react-redux";
import {setPageInfo, setSelectedItemType} from "../../../store/filters";
import {addBasketItem} from "../../../store/basket";
import {MarketLoader} from "../../../components/loader";

type ProductsType = {
    products: { data: Product[], totalAmount: number };
    loading: boolean
}

export enum ITEM_TYPE {
    MUG = 'mug',
    SHIRT = 'shirt'
}

export const Products: FunctionComponent<ProductsType> = (props) => {
    const dispatch = useDispatch();
    const {products, loading} = props;
    const [selectedFilter, setSelectedFilter] = useState<ITEM_TYPE | null>(null)

    const onHandlePageChange = (page: any, limit: any) => {
        dispatch(setPageInfo({page, limit}))
    }

    const onAddProduct = (product: Product) => {
        return () => {
            dispatch(addBasketItem(product))
        }
    }

    const onHandleFilterProduct = (type: ITEM_TYPE) => {
        return () => {
            const value = type === selectedFilter ? null : type;
            setSelectedFilter(value)
            dispatch(setSelectedItemType(value))
        }
    }

    const renderProducts = () => {
        if (!products || !products.totalAmount || loading) {
            return <MarketLoader/>
        }
        if(products.data.length === 0){
            return <span>No product found.</span>
        }
        return products.data.map((item, i) => <ProductItem onProductAddClicked={onAddProduct(item)} key={i}
                                                           data={item}/>)
    }

    return <div className="products-container">
        <div className="products-header">
            <label>Products</label>
        </div>
        <div className="products-button-filters">
            <Button onClick={onHandleFilterProduct(ITEM_TYPE.MUG)}
                    className={selectedFilter && selectedFilter === ITEM_TYPE.MUG ? 'market-button' : 'market-outlined-button'}>mug
            </Button>
            <Button onClick={onHandleFilterProduct(ITEM_TYPE.SHIRT)}
                    className={selectedFilter && selectedFilter === ITEM_TYPE.SHIRT ? 'market-button' : 'market-outlined-button'}>shirt
            </Button>
        </div>
        <div className="products-item-container">
            {renderProducts()}
        </div>
        <div className="products-paginator">
            <Pagination pageSize={16} onChange={onHandlePageChange} total={products ? products.totalAmount : 0}
                        pageSizeOptions={[]}/>
        </div>
    </div>
}
