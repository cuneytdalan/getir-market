import {FunctionComponent} from "react";
import {Button} from "antd";
import {Product} from "../../../../types/products";

export type ProductItemData = {
    label: string;
    price: any;
}

type ProductItemType = {
    data: Product,
    onProductAddClicked: any
}

export const ProductItem: FunctionComponent<ProductItemType> = (props) => {
    const {data, onProductAddClicked} = props;

    return <div className="product-item-container">
        <div className="product-item-image">
            <div className="item-image">
                <img alt={data.name} src={`https://picsum.photos/id/${Math.floor(Math.random() * (100)) + 1}/200/200`}/>
            </div>
        </div>
        <div className="product-item-price">
            ₺ {data.price}
        </div>
        <div className="product-item-label">
            {data.name}
        </div>
        <div className="product-item-button">
            <Button onClick={onProductAddClicked}>Add</Button>
        </div>
    </div>
}
