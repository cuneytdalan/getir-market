import React from 'react';
import './App.scss';
import {GeneralPageLayout} from "./components/layout/generalPageLayout";
import {GeneralContent} from "./components/layout/generalContent";
import {MarketPage} from "./page/market";
import {Provider} from "react-redux";
import {store} from "./store/store";

export default function App() {
    document.title = "Getir Market";
    return <Provider store={store}>
        <GeneralPageLayout>
            <GeneralContent>
                <MarketPage/>
            </GeneralContent>
        </GeneralPageLayout>
    </Provider>
}
