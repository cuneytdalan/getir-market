import {ProductStoreType} from "../store/basket";
import {Product} from "../types/products";

export function getTotalPriceOfBasketItems(basket: { list: ProductStoreType[], totalPrice: number }) {
    basket.totalPrice = 0;
    basket.list.forEach((listItem: { item: Product, amount: number }) => {
        basket.totalPrice += (listItem.amount * listItem.item.price)
    });
    basket.totalPrice = Number(basket.totalPrice.toFixed(2));
    return basket;
}
