export type FilterOptions<M> = {
    label: string;
    value: M
}
