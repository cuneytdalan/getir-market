import {ITEM_TYPE} from "../page/market/products";

export type ProductsLazyLoadOptions = {
    selectedSorting?: SelectedSorting;
    pageInfo: PageInfo;
    selectedBrands?: [],
    selectedTags?: [],
    selectedItemType: ITEM_TYPE
}

export type SelectedSorting = {
    order: string;
    sort: string;
}
export type PageInfo = {
    page: number;
    limit: number;
}
