import {ProductsLazyLoadOptions} from "../types/pageOptions";
import {API_URL, ITEMS_API} from "../constants";

export const getCompaniesApi = () => {
    return fetch(`${API_URL}/companies`, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    )
        .then(function (response) {
            return response.json();
        })
        .then(function (response) {
            return response
        });
}

export const getTagsApi = () => {
    return fetch(`${API_URL}/tags`, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    )
        .then(function (response) {
            return response.json();
        })
        .then(function (response) {
            return response
        });
}

export const getItemsApi = (options: ProductsLazyLoadOptions) => {
    let totalAmount: any;
    let apiUrl: string = `${API_URL}${ITEMS_API}`;
    let apiParams: string = '';
    if (options.selectedBrands) {
        options.selectedBrands.forEach(brand => {
            apiParams += `${!apiParams ? '?' : ''}manufacturer=${brand}&`
        })
    }
    if (options.selectedTags && options.selectedTags.length > 0) {
        apiParams += `${!apiParams ? '?' : ''}tags_like=${options.selectedTags.toString()}&`
    }
    if (options.selectedItemType) {
        apiParams += `${!apiParams ? '?' : ''}itemType=${options.selectedItemType}&`
    }
    if (options.pageInfo) {
        apiParams += `${!apiParams ? '?' : ''}_page=${options.pageInfo.page}&_limit=${options.pageInfo.limit}`;
    }
    if (options.selectedSorting && options.selectedSorting.sort) {
        apiParams += `&_sort=${options.selectedSorting.sort}&_order=${options.selectedSorting.order}`
    }
    apiUrl += apiParams
    return fetch(apiUrl, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    )
        .then(function (response) {
            totalAmount = response.headers.get('X-Total-Count');
            return response.json();
        })
        .then(function (response) {
            return {data: response, totalAmount};
        });
}
